#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream>
#include <string.h>
using namespace std;

class Cliente{
    
    private:
        string nome;
        string rg;
        string data_nascimento;
        string senha;
        bool socio;
        
    public:
        Cliente();
        Cliente(string nome,string rg,string data_nascimento,string senha, bool socio);
        ~Cliente();

        void set_cliente_dados(string nome, string rg, string data_nascimento, string senha);
        string get_nome();
        string get_rg();
        string get_data_nascimento();
        string get_senha();
        
        void set_socio(bool socio);
        bool get_socio();

};

#endif